import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import '../../widgets/matrix.dart';
import 'matrix_sdk_extensions.dart/matrix_locals.dart';

/// Erweiterungsmethoden für den Matrix-Raum.
extension GRoomExtension on Room {
  /// Gibt den Anzeigenamen des Raums zurück. Für den Sonderfall, dass sich genau zwei Personen
  /// im Raum befinden, wird der Anzeigename des Gesprächspartners angezeigt. Der BuildContext [context] wird benötigt,
  /// um Benutzer-ID und Übersetzungen zu ermitteln.
  g_getDisplayName(BuildContext context) {
    if (!isDirectChat) {
      final participants = getParticipants([Membership.join, Membership.invite]);
      if (participants.length == 2) {
        final myUserId = Matrix.of(context).client.userID;
        final otherParticipant = participants.firstWhere((user) => user.id != myUserId);
        return otherParticipant.displayName ?? otherParticipant.id;
      }
    }
    return getLocalizedDisplayname(MatrixLocals(L10n.of(context)!));
  }
}
