import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:fluffychat/config/app_config.dart';
import 'package:fluffychat/utils/platform_infos.dart';
import 'package:fluffychat/widgets/layouts/login_scaffold.dart';
import 'homeserver_picker_gauss.dart';

class HomeserverPickerGaussView extends StatelessWidget {
  final HomeserverPickerGaussController controller;

  const HomeserverPickerGaussView(this.controller, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LoginScaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: controller.restoreBackup,
            tooltip: L10n.of(context)!.hydrate,
            color: Colors.white,
            icon: const Icon(Icons.restore_outlined),
          ),
          IconButton(
            tooltip: L10n.of(context)!.privacy,
            onPressed: () => launch(AppConfig.privacyUrl),
            color: Colors.white,
            icon: const Icon(Icons.shield_outlined),
          ),
          IconButton(
            tooltip: L10n.of(context)!.about,
            onPressed: () => PlatformInfos.showDialog(context),
            color: Colors.white,
            icon: const Icon(Icons.info_outlined),
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: ListView(children: [
                Container(
                    padding: const EdgeInsets.all(12),
                    child: const Text(
                        "Bitte QR-Code für die Konfiguration scannen")),
                Container(
                  padding: const EdgeInsets.all(12),
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: controller.startQRConfigReader,
                    child: const Icon(Icons.qr_code_scanner),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 12, right: 12, top: 12),
                  child: Text(controller.config == null
                      ? ""
                      : "Server: ${controller.config?.homeserver}"),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 12, right: 12),
                  child: Text(controller.config == null
                      ? ""
                      : "User: ${controller.config?.username}"),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 12, right: 12),
                  child: Text(controller.config == null ? "" : "PW: ****"),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 12, right: 12),
                  child: Text(controller.error),
                ),
              ]),
            ),
            Container(
              padding: const EdgeInsets.all(12),
              width: double.infinity,
              child: Hero(
                tag: 'loginButton',
                child: ElevatedButton(
                  onPressed: (controller.config?.isOk() ?? false)
                      ? controller.login
                      : null,
                  child: const Text("Login"),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
