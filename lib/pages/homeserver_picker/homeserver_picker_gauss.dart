import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart';

import 'package:fluffychat/pages/homeserver_picker/homeserver_picker_gauss_view.dart';
import 'package:fluffychat/widgets/matrix.dart';
import '../../utils/platform_infos.dart';

import '../new_private_chat/qr_scanner_modal.dart';

class HomeserverPickerGauss extends StatefulWidget {
  const HomeserverPickerGauss({Key? key}) : super(key: key);

  @override
  HomeserverPickerGaussController createState() =>
      HomeserverPickerGaussController();
}

class HomeserverPickerGaussController extends State<HomeserverPickerGauss> {
  SoPartLoginConfig? config = null;
  String error = "";

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Matrix.of(context).navigatorContext = context;
    return HomeserverPickerGaussView(this);
  }

  void reset() {
    setState(() {
      error = "";
      config = null;
    });
  }

  void showError(String msg) {
    setState(() {
      error = msg;
    });
  }

  Future<void> restoreBackup() async {
    await showFutureLoadingDialog(
        context: context,
        future: () async {
          try {
            final file = await FilePickerCross.importFromStorage(
                fileExtension: '.fluffybackup');
            final client = Matrix.of(context).getLoginClient();
            await client.importDump(file.toString());
            Matrix.of(context).initMatrix();
          } catch (e, s) {
            Logs().e('Future error:', e, s);
          }
        });
  }

  Future<void> startQRConfigReader() async {
    reset();
    await showModalBottomSheet(
        context: context,
        useRootNavigator: false,
        //useSafeArea: false,
        builder: (_) => QrScannerModal(readAction: (context, scanData) {
              munchQRConfigData(scanData.code ?? "");
            }));
  }

  void munchQRConfigData(String text) {
    bool ok = true;
    ok &= text.startsWith("SoPart.IM");
    String url = "";
    String accessToken = "";
    if (ok) {
      final lines = const LineSplitter().convert(text);
      for (final line in lines) {
        final l = line.trim();
        if (l.startsWith("URL:")) {
          url = l.substring("URL:".length);
        } else if (l.startsWith("AccessToken:")) {
          accessToken = l.substring("AccessToken:".length);
        }
      }
      ok &= url.isNotEmpty && accessToken.isNotEmpty;
      fetchUserData(url, accessToken);
    }
    if (!ok) {
      showError("Kein gültiger SoPart-QR-Code:\n$text");
    }
  }

  Future<void> fetchUserData(String url, String accessToken) async {
    Uri tmp;
    try {
      tmp = Uri.parse("https://$url");
    } on FormatException {
      showError("Ungültige URL in QR-Code: $url");
      return;
    }
    final params = {'ACCESS_TOKEN': accessToken};
    final uri = Uri.https(tmp.host, tmp.path, params);
    http.Response resp;
    try {
      resp = await http.get(uri);
    } catch (ex) {
      showError("Fehler beim Holen der Login-Daten:\n${ex.toString()}");
      return;
    }
    if (resp.statusCode < 200 || resp.statusCode >= 300) {
      showError(
          "Fehler beim Holen der Login-Daten: HTTP Error ${resp.statusCode}");
      return;
    }
    try {
      final values = jsonDecode(resp.body);
      setState(() => config = SoPartLoginConfig.fromJson(values));
      if (config?.homeserver.isEmpty ?? true) {
        showError("Der Wert für 'Homeserver' ist leer");
      } else if (config?.username.isEmpty ?? true) {
        showError("Der Wert für 'Username' ist leer");
      } else if (config?.password.isEmpty ?? true) {
        showError("Der Wert für 'Password' ist leer");
      }
    } catch (ex) {
      showError("Konnte abgeholte Login-Daten nicht dekodieren:\n${resp.body}");
      return;
    }
  }

  Future<void> login() async {
    final matrix = Matrix.of(context);
    try {
      matrix.initMatrix();
      final client = matrix.getLoginClient();
      client.checkHomeserver(Uri.parse("https://${config?.homeserver}"));
      final username = config?.username ?? "";
      final identifier = AuthenticationUserIdentifier(user: username);
      await client.login(LoginType.mLoginPassword,
          identifier: identifier,
          // To stay compatible with older server versions
          // ignore: deprecated_member_use
          user: identifier.type == AuthenticationIdentifierTypes.userId
              ? username
              : null,
          password: config?.password ?? "",
          initialDeviceDisplayName: PlatformInfos.clientName);
    } catch (ex) {
      showError("Fehler beim Login:\n${ex.toString()}");
    }
  }
}

class SoPartLoginConfig {
  final String homeserver;
  final String username;
  final String password;

  SoPartLoginConfig(this.homeserver, this.username, this.password);

  SoPartLoginConfig.fromJson(Map<String, dynamic> json)
      : homeserver = json['Homeserver'],
        username = json['Username'],
        password = json['Password'];

  bool isOk() {
    return homeserver.isNotEmpty && username.isNotEmpty && password.isNotEmpty;
  }
}
